set :build_dir, 'public'
activate :directory_indexes
activate :asciidoc, base_dir: :source, template_dirs: 'asciidoc_templates', template_engine: :erb
ignore 'stylesheets/sass/**/*.sass'

configure :build do
  activate :minify_css
  activate :minify_javascript
end

helpers do
  def ad(body)
    Asciidoctor.convert(body.split(/^---/)[-1], standalone: false, safe: :safe, template_dirs: ['asciidoc_templates'], template_engine: 'erb')
  end
end